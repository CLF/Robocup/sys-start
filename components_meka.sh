#############################################################################
# COMPONENT LIST MEKA
##############################################################################

if [ "$VDEMO_simulation" = true ]
then
##############################################################################
# VIRTUAL ROBOT
##############################################################################


# SIM
export VDEMO_component_gazebo_upload="roslaunch,$lower, -t upload -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=meka_upload.launch -l -d -1 -L 2:"
export VDEMO_component_gazebo_floka_upload="roslaunch,$lower, -t upload_floka -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=meka_floka_upload.launch -l -d -1 -L 2:"
export VDEMO_component_gazebo_control="roslaunch,$lower, -t control -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=meka_controller.launch -v TOPIC_TO_CHECK=/meka_roscontrol/joint_states -l -d -1 -L 3 -w 10:"
export VDEMO_component_set_zlift="ros_control_pub,$upper, -t set_z_lift -g sim -v JOINT=zlift_j0 -v POSITION=0.3 -l -d -1 -L 4:"

export VDEMO_component_jtc_rqt="rosrun,$basepc, -x -t jtc_rqt -g ros -v PACKAGE=rqt_gui -v BINARY=rqt_gui -v ARGS='-s joint_trajectory_controller'-L 4: "

# Floka
export VDEMO_component_xsc3="xsc3,$basepc,-l -t xsc3_VIRTUAL -v MODE=virtual -w 3 -g floka -L 3:"
export VDEMO_component_flokasim="flokasim,$basepc,-l -t floka_sim -x -n -w 5 -g sim -L 2:"

export VDEMO_component_group_robot="
sim:
$VDEMO_component_gazebo_gui
$VDEMO_component_gazebo_upload
$VDEMO_component_gazebo_control
default:
$VDEMO_component_jtc_rqt
$VDEMO_component_set_zlift
"

export VDEMO_component_group_floka="
floka:
$VDEMO_component_xsc3
$VDEMO_component_flokasim
default:
"

##############################################################################
# REAL ROBOT
##############################################################################
else

# Floka
export VDEMO_component_xsc3_config="rosparam_load,$upper, -t xsc3_config_upl -g hw -v CONFIG=\"${prefix}/share/meka_bringup/config/xsc3_fneck_lblink_config.yaml\" -L 1:"
export VDEMO_component_xsc3="xsc3,$laptop,-l -t xsc3_REAL -w 3 -g floka -n -L 3:"
export VDEMO_component_xsc3_sim="xsc3,$basepc,-l -t xsc3_VIRTUAL -v MODE=virtual -w 3 -g sim -n -L 3:"
export VDEMO_component_flokasim="flokasim,$basepc,-l -t floka_sim -x -n -w 5 -g sim -n -L 2:"
export VDEMO_component_ximea_cam_right="ros_ximea_cam,$gpu,-l -t cam_grabber_right -g cam -L 5 -v CAMERA=right -n -x:"
export VDEMO_component_ximea_cam_left="ros_ximea_cam,$gpu,-l -t cam_grabber_left -g cam -L 5 -v CAMERA=left -n -x:"

# ROS
export VDEMO_component_ros_upload_state_pub="meka_upload,$lower, -t meka_state_pub -g ros -L 1 -w 6: "

# SENSORS
#TODO: fix laser launch files and meka_config and then readd lasers to group
export VDEMO_component_hokuyo_laser="rosrun,$upper, -t front_laser -g hw -v PACKAGE=urg_node -v BINARY=urg_node -v ARGS=\"_serial_port\:=/dev/hokuyo\" -L 3:"
export VDEMO_component_sick_laser="roslaunch,$upper, -t back_laser -g hw -v PACKAGE=sick_tim -v LAUNCHFILE=sick_tim571_2050101.launch -L 3:"
export VDEMO_component_xtion2="roslaunch,$upper, -t xtion_cam -g hw -v PACKAGE=tobi_bringup -v LAUNCHFILE=xtion2.launch -L 1 -n: "
export VDEMO_component_realsense_face="roslaunch,$upper, -t realsense_face -g hw -v PACKAGE=meka_bringup -v LAUNCHFILE=realsense_face.launch -L 1: "
export VDEMO_component_realsense_face_calib="rosrun,$lower, -t rs_face_tf -g hw -v PACKAGE=tf2_ros -v BINARY=static_transform_publisher -v NODE_TO_CHECK=static_transform_publisher -v ARGS=\"0.08 0.03 0.21 0.02 -0.34 0 upper realsense_face_link\" -L 2: "
export VDEMO_component_realsense_hands="roslaunch,$gpu, -t realsense_hands -g hw -v PACKAGE=meka_bringup -v LAUNCHFILE=realsense_hands.launch -L 1: "
export VDEMO_component_realsense_hands_calib="rosrun,$lower, -t rs_hands_tf -g hw -v PACKAGE=tf2_ros -v BINARY=static_transform_publisher -v NODE_TO_CHECK=static_transform_publisher -v ARGS=\"0.08 0.01 0.26 0.07 0.33 0.0 upper realsense_hands_link\" -L 2: "

# M3
export VDEMO_component_server_run_shm="m3rt_server_run_shm,$lower, -g m3 -L 2 -w 26 -l: "
export VDEMO_component_ros_upload_ctrl="ros_launch_blocking,$lower, -t ros_upload_ctrl -g m3 -v ROS_PACKAGE=m3ros_control -v ROS_LAUNCH=meka_controllers_upload.launch -L 1 -w 7: "
export VDEMO_component_ros_control_state_manager="rosrun,$lower, -t m3ctrl_state_manager -g m3 -v PACKAGE=m3ros_control -v BINARY=state_manager.py -v NODE_TO_CHECK=meka_state_manager -L 3 -w 5: "
export VDEMO_component_ros_publisher_js="m3ros_control_controller,$lower, -t ros_jspub -g m3 -v M3ROS_CONTROLLER=meka_controllers_state.launch -v TOPIC_TO_CHECK=/meka_roscontrol/joint_states -L 3 -w 7: "
export VDEMO_component_dold_driver="rosrun,$upper, -t dold_driver -g hw -v PACKAGE=dold_driver -v BINARY=dold_driver -v ARGS=\"/dev/dold 115200\" -v NODE_TO_CHECK=teensy_tactile -L 4: "
export VDEMO_component_dold_interface="rosrun,$upper, -t dold_meka_interface -g hw -v PACKAGE=dold_meka_interface -v BINARY=dold_meka_interface.py -L 4: "
export VDEMO_component_rqt="rosrun,$basepc, -x -t rqt_gui -g ros -v PACKAGE=rqt_gui -v BINARY=rqt_gui -L 4: "
# CALIB
export VDEMO_component_omnibase_calibrate="m3dev_omnibase_calibrate,$lower, -g calib -L 3 -w 3 -n -d -1: "
export VDEMO_component_dummy_odom="rosrun,$lower, -t dummy_odom -g calib -v PACKAGE=tf -v BINARY=static_transform_publisher -v NODE_TO_CHECK=static_transform_publisher -v ARGS=\"0 0 0 0 0 0 odom base_link 20\" -L 3: "
# PUB
export VDEMO_component_meka_ros_publisher="rosrun,$lower, -t meka_ros_publisher -g pub -v PACKAGE=meka_ros_publisher -v BINARY=meka_ros_publisher -v ARGS=\"--server\" -L 3 -w 10: "
export VDEMO_component_ros_pwr38_pub="meka_ros_pub,$upper, -t pwr38 -g pub -v COMP=m3pwr_pwr038 -v FIELD=bus_voltage -v TYPE=Floats -v HZ=1 -L 9: "
export VDEMO_component_ros_pwr42_pub="meka_ros_pub,$upper, -t pwr42 -g pub -v COMP=m3pwr_pwr042 -v FIELD=bus_voltage -v TYPE=Floats -v HZ=1 -L 9: "
export VDEMO_component_ros_fts29_pub="meka_ros_pub,$upper, -t fts29 -g pub -v COMP=m3loadx6_ma29_l0 -v FIELD=wrench -v TYPE=WrenchStamped -v HZ=100 -L 9: "
export VDEMO_component_ros_fts30_pub="meka_ros_pub,$upper, -t fts30 -g pub -v COMP=m3loadx6_ma30_l0 -v FIELD=wrench -v TYPE=WrenchStamped -v HZ=100 -L 9: "
export VDEMO_component_force_helper="rosrun,$upper, -x -t force_helper -g pub -v PACKAGE=force_helper -v BINARY=force_helper_node.py -L 9: "

# RECORDING
export VDEMO_component_usb_cam_extern="roslaunch,$extern, -t usb_cam_extern -v PACKAGE=meka_bringup -v LAUNCHFILE=remote_logitech.launch -g remote -L 9:"
export VDEMO_component_usb_cam_top_extern="roslaunch,$extern, -t usb_cam_top -v PACKAGE=meka_bringup -v LAUNCHFILE=remote_logitech_top.launch -g remote -L 9:"
export VDEMO_component_floka_right_extern="roslaunch,$extern, -t floka_right_remote -v PACKAGE=meka_bringup -v LAUNCHFILE=floka_camera_right.launch -v TOPIC_TO_CHECK=/floka/eye_right/camera_info -g remote -L 9:"
export VDEMO_component_rosbag_extern="rosbag_extern,$extern, -t rosbag_remote -g remote -L 9 -l: "
export VDEMO_component_bart_extern="roslaunch,$extern, -t bart_remote -v PACKAGE=meka_bringup -v LAUNCHFILE=bart.launch -g remote -L 9:"

export VDEMO_component_group_robot="
hardware:
$VDEMO_component_xtion2
$VDEMO_component_dold_driver
$VDEMO_component_dold_interface
$VDEMO_component_rqt
$VDEMO_component_server_run_shm
$VDEMO_component_ros_upload_ctrl
$VDEMO_component_ros_control_state_manager
$VDEMO_component_ros_publisher_js
$VDEMO_component_omnibase_calibrate
$VDEMO_component_dummy_odom
$VDEMO_component_ros_upload_state_pub
$VDEMO_component_realsense_face
$VDEMO_component_realsense_face_calib
$VDEMO_component_realsense_hands
$VDEMO_component_realsense_hands_calib
publisher:
$VDEMO_component_meka_ros_publisher
$VDEMO_component_ros_publisher
$VDEMO_component_ros_pwr38_pub
$VDEMO_component_ros_pwr42_pub
$VDEMO_component_ros_fts29_pub
$VDEMO_component_ros_fts30_pub
$VDEMO_component_force_helper
default:
"

export VDEMO_component_group_floka="
floka:
$VDEMO_component_xsc3_config
$VDEMO_component_xsc3
$VDEMO_component_xsc3_sim
$VDEMO_component_flokasim
$VDEMO_component_ximea_cam_right
$VDEMO_component_ximea_cam_left
default:
"

export VDEMO_component_group_remote="
remote:
$VDEMO_component_usb_cam_extern
$VDEMO_component_usb_cam_top_extern
$VDEMO_component_floka_right_extern
$VDEMO_component_rosbag_extern
$VDEMO_component_bart_extern
default:
"

fi

##############################################################################
# Additional Meka components
##############################################################################

# FLOKA
export VDEMO_component_mary_server="mary_server,$upper,-l -t mary_server -w 5 -g floka -L 1:"
export VDEMO_component_mary_tts_provider="mary_tts_provider,$upper,-l -t mary_tts_provider -w 5 -g floka -L 1:"

export VDEMO_component_xsc3_health_monitor="xsc3_health_monitor,$basepc,-l -d -1 -t xsc3_health_monitor -w 5 -n -g floka -L 2:"
export VDEMO_component_xsc3_control="xsc3_control,$basepc,-n -l -t xsc3_control -w 3 -g floka -L 2 -x:"
export VDEMO_component_state_pub="roslaunch,$upper,-l -t floka_robst_pub -g floka -v PACKAGE=meka_bringup -v LAUNCHFILE=floka_jointstate_publisher.launch -L 6 -x:"

export VDEMO_component_group_floka="
$VDEMO_component_group_floka
floka:
$VDEMO_component_mary_server
$VDEMO_component_state_pub
$VDEMO_component_mary_tts_provider
$VDEMO_component_xsc3_health_monitor
$VDEMO_component_xsc3_control
$VDEMO_component_hlrc_server
$VDEMO_component_hlrc_test_gui
load_urdf,$upper,-l -t floka_urdf -g tf -L 6 -x:
rosrun,$upper,-t floka_to_base -g tf -v PACKAGE=tf2_ros -v BINARY=static_transform_publisher -v ARGS=\"0.0 0.0 0.2757 0 0 0 /upper /floka_base_link\" -v NODE_TO_CHECK=static -L 6 -x:
default:
"

# VIZ
export VDEMO_component_rviz_grasp="ros_rviz,$basepc, -t rviz_grasp -x -g viz -v RVIZ_CONFIG=${prefix}/share/meka_bie_moveit_config/launch/moveit.rviz -n -L 5: "
export VDEMO_component_rviz_nav="ros_rviz,$basepc, -t rviz_nav -x -g viz -v RVIZ_CONFIG=${prefix}/share/meka_bringup/config/navigation.rviz -n -L 5: "

# NAV
export VDEMO_component_navigation="roslaunch,$upper, -t ros_navigation -g nav -v PACKAGE=meka_2dnav -v LAUNCHFILE=navigation.launch -L 4:"
export VDEMO_component_navigation_efficient="roslaunch,$lower, -t ros_nav_eff -g nav -v PACKAGE=meka_2dnav -v LAUNCHFILE=meka_navigation_mob_nomap_efficient.launch -L 4:"
export VDEMO_component_laser="roslaunch,$upper, -t ros_laser -g nav -v PACKAGE=meka_2dnav -v LAUNCHFILE=meka_config.launch -L 4:"
export VDEMO_component_ros4rsb="ros4rsb,$upper, -t ros4rsb -g nav -L 4 -w 3:"
export VDEMO_component_velocity_smoother="roslaunch,$upper, -t velocity_smoother -g nav -v PACKAGE=meka_velocity_smoother -v LAUNCHFILE=standalone.launch -L 3:"

export VDEMO_component_group_navigation="
navigation:
$VDEMO_component_laser
$VDEMO_component_navigation_efficient
$VDEMO_component_velocity_smoother
$VDEMO_component_rviz_nav
default:
"

# POSTURE
export VDEMO_component_posture_execution="rosrun,$lower, -t posture_execution -g posture -v PACKAGE=posture_execution -v BINARY=posture_execution_node.py -v ARGS=\"--postures ${prefix}/share/posture_execution/config/postures.yml\" -L 4: "
export VDEMO_component_posture_recorder="rosrun,$lower, -t posture_recorder -g posture -v PACKAGE=posture_recorder -v BINARY=posture_recorder.py -n -L 5: "

export VDEMO_component_group_postures="
postures:
$VDEMO_component_posture_recorder
$VDEMO_component_posture_execution
execute_posture,$upper, -t posture_waiting -v POSTURE=waiting -n -L 5 -w 1 :
execute_posture,$upper, -t posture_nodding -v POSTURE=nodding -n -L 5 -w 1 :
execute_posture,$upper, -t posture_pointing_kitchen -v POSTURE=pointing_kitchen -n -L 5 -w 1 :
execute_posture,$upper, -t posture_pointing_screen -v POSTURE=pointing_screen -n -L 5 -w 1 :
execute_posture,$upper, -t posture_time -v POSTURE=time -n -L 5 -w 1 :
execute_posture,$upper, -t posture_welcoming -v POSTURE=welcoming -n -L 5 -w 1 :
"

# OBJECT RECOGNITION & SEGMENTATION
export VDEMO_component_tensorflow="meka_tensorflow, $gpu, -t tensorflow -g object -v PACKAGE=tensorflow_ros -v BINARY=object_recognition_node -v ARGS=\"_graph_path\:=\${OBJECT_REC_GRAPH} _labels_path\:=\${OBJECT_REC_LABELS} \" -L 7:"
export VDEMO_component_segmentation="segmentation,$upper, -t ros_segmentation -g object -L 7 -l:"
export VDEMO_component_classify_bridge="classify_bridge,$upper, -t classify_bridge -g object -L 7 -l:"
export VDEMO_component_psm="rosrun,$upper, -t psm -g object -v PACKAGE=planning_scene_manager -v BINARY=planning_scene_manager -L 7 -l:"

# GRASP
export VDEMO_component_moveit_augmented="roslaunch,$upper, -t moveit_augmented -g grasp -v PACKAGE=meka_bie_moveit_config -v LAUNCHFILE=move_group.launch -v ARGS=\"augmented\:=true\" -L 5: "
export VDEMO_component_agni_grasping="roslaunch,$upper, -t agni_grasping -g grasp -v PACKAGE=meka_bringup -v LAUNCHFILE=grasp_provider_meka.launch -L 6: "
export VDEMO_component_object_fitter="roslaunch,$upper, -t object_fitter -g grasp -v PACKAGE=meka_bringup -v LAUNCHFILE=object_fitter.launch -v ARGS=\"rsb_transport_socket_enabled\:=0 rsb_transport_spread_enabled\:=1 table_safety_margin\:=0.015\" -L 7: "

export VDEMO_component_group_manipulation="
grasping:
$VDEMO_component_segmentation
$VDEMO_component_tensorflow
$VDEMO_component_classify_bridge
$VDEMO_component_psm
$VDEMO_component_object_fitter
$VDEMO_component_agni_grasping
$VDEMO_component_moveit_augmented
$VDEMO_component_rviz_grasp
$VDEMO_component_rqt_grasp_viewer
default:
"

# RC
export VDEMO_component_joypair="rosrun,$upper, -t joy_pair -g rc -v PACKAGE=ps3joy -v BINARY=ps3joy.py -l -w 5 -L 9 -n: "
export VDEMO_component_joylaunch="roslaunch,$upper, -t joy_launch -g rc -v PACKAGE=ps3joy -v LAUNCHFILE=ps3.launch -l -L 9 -w 5 -n: "
export VDEMO_component_joyteleop="roslaunch,$upper, -t teleop_joy -g rc -v PACKAGE=teleop_twist_joy -v LAUNCHFILE=teleop.launch -L 9 -n -l -n: "
export VDEMO_component_keyboard_teleop="rosrun,$upper, -t keyboard_teleop -g rc -l -n -v PACKAGE=keyboard_teleop -v BINARY=keyboard_teleop.py -L 9:"
export VDEMO_component_odom_relay="rosrun,$upper, -t odom_relay -g rc -v PACKAGE=topic_tools -v BINARY=relay -v ARGS=\"/roscontrol_base_controller/odom /odom\" -v NODE_TO_CHECK=odom_relay -L 3: "

export VDEMO_component_group_rc="
rc:
$VDEMO_component_joypair
$VDEMO_component_joylaunch
$VDEMO_component_joyteleop
$VDEMO_component_keyboard_teleop
$VDEMO_component_odom_relay
default:
"

# HUMOTION 
export USB_CAM_NR=0
export DLIB_MODEL=${prefix}/share/dlib/shape_predictor_68_face_landmarks.dat
export VDEMO_component_hlrc_server="hlrc_server,$lower,-l -t hlrc_server -n -w 5 -g humotion -L 6 -v BASE_SCOPE=${HUMOTION_TOPIC} -v SOUND_OUTPUT=none :"
export VDEMO_component_hlrc_test_gui="hlrc_test_gui,$upper,-l -n -L 7 -g humotion -v BASE_SCOPE=${HUMOTION_TOPIC} :"
export VDEMO_component_hlrc_test_relative_gaze="hlrc_test_relative_gaze,$upper,-l -L 7 -g humotion -v BASE_SCOPE=${HUMOTION_TOPIC} -n :"
export VDEMO_component_humotion_server="humotion_server,$lower,-l -n -g humotion -L 5 -v HUMOTION_TOPIC=${HUMOTION_TOPIC} -v CONTROLSTATES_TOPIC=/roscontrol_state_manager/state -v JOINTSTATES_TOPIC=/joint_states -v CONTROL_TOPIC=/roscontrol/head_position_trajectory_controller/command :"

export VDEMO_component_group_humotion="
humotion:
$VDEMO_component_hlrc_server
$VDEMO_component_hlrc_test_gui
$VDEMO_component_hlrc_test_relative_gaze
default:
"

# HRI
export VDEMO_component_force_guiding="roslaunch,$upper, -t guiding -g hri -l -v PACKAGE=force_guiding -v LAUNCHFILE=guiding.launch -L 9:"
export VDEMO_component_hand_shaker="rosrun,$upper, -t hand_shaker -g hri -v PACKAGE=hand_shaker -v BINARY=hand_shaker_node.py -v ARGS=\"--posture=${prefix}/share/posture_execution/config/postures.yml\" -L 4: "
export VDEMO_component_hand_over="rosrun,$upper, -t hand_over -g hri -v PACKAGE=hand_over -v BINARY=hand_over_node.py -v ARGS=\"--posture=${prefix}/share/posture_execution/config/postures_nonverbal_hand_over.yml\" -L 4: "

export VDEMO_component_group_hri="
hri:
$VDEMO_component_hand_shaker
$VDEMO_component_hand_over
$VDEMO_component_force_guiding 
default:
"

# CONTROLLER DEBUG
export VDEMO_component_ros_controller_arm="m3ros_control_controller,$lower, -t ros_armctrl -g ctrl -v M3ROS_CONTROLLER=meka_controllers_arms.launch -v TOPIC_TO_CHECK=/meka_roscontrol/left_arm_position_trajectory_controller/state -n -L 4 -w 7: "
export VDEMO_component_ros_controller_stiff="m3ros_control_controller,$lower, -t ros_stiff -g ctrl -v M3ROS_CONTROLLER=meka_controllers_stiffness.launch -v TOPIC_TO_CHECK=/meka_roscontrol/joint_states -L 4 -w 7:"
export VDEMO_component_ros_controller_hand="m3ros_control_controller,$lower, -t ros_handctrl -g ctrl -v M3ROS_CONTROLLER=meka_controllers_hands.launch -v TOPIC_TO_CHECK=/meka_roscontrol/left_hand_position_trajectory_controller/state -n -L 4 -w 7: "
export VDEMO_component_ros_controller_body="m3ros_control_controller,$lower, -t ros_bodyctrl -g ctrl -v M3ROS_CONTROLLER=meka_controllers_body.launch -v TOPIC_TO_CHECK=/meka_roscontrol/torso_position_trajectory_controller/state -n -L 4 -w 7: "
export VDEMO_component_ros_controller_head="m3ros_control_controller,$lower, -t ros_headctrl -g ctrl -v M3ROS_CONTROLLER=meka_controllers_head.launch -v TOPIC_TO_CHECK=/meka_roscontrol/head_position_trajectory_controller/state -n -L 4 -w 7: "
export VDEMO_component_ros_controller_zlift="m3ros_control_controller,$lower, -t ros_zliftctrl -g ctrl -v M3ROS_CONTROLLER=meka_controllers_zlift.launch -v TOPIC_TO_CHECK=/meka_roscontrol/zlift_position_trajectory_controller/state -n -L 4 -w 7: "

export VDEMO_component_group_ros_debug_utils="
debug:
$VDEMO_component_ros_controller_arm
$VDEMO_component_ros_controller_hand
$VDEMO_component_ros_controller_body
$VDEMO_component_ros_controller_zlift
$VDEMO_component_ros_controller_head
$VDEMO_component_ros_controller_stiff
default:
"



# PEOPLE
export VDEMO_component_people_tracker="roslaunch,$lower, -t people_tracker -g people -v PACKAGE=tobi_bringup -v LAUNCHFILE=people_tracker_robot_meka.launch -n -L 4: "
export VDEMO_component_leg_based_people_tracker="roslaunch,$lower, -t people_tracker -g people -v PACKAGE=meka_bringup -v LAUNCHFILE=leg_based_person_tracking.launch -L 4: "
export VDEMO_component_openpose="rosrun,$gpu, -t openpose -g people -v PACKAGE=openpose_ros -v BINARY=detect_people_server -v NODE_TO_CHECK=/detect_people_server -v ARGS=\"_models_folder\:=${prefix}/share/openpose/models/ _camera_frame\:=xtion_rgb_optical_frame  _visualize\:=false \" -L 7:"
export VDEMO_component_hace="roslaunch,$gpu, -t hace -g people -v PACKAGE=hace_core -v LAUNCHFILE=floka_torso_cam.launch -L 4: "
export VDEMO_component_handtracking="roslaunch,$gpu, -t handtracking -g people -v PACKAGE=handtracking -v LAUNCHFILE=meka_nodelet.launch -L 4: "
export VDEMO_component_otpprediction="rosrun,$gpu, -t otppredict -g people -v PACKAGE=prediction -v BINARY=prediction -L 4: "

export VDEMO_component_group_people="
people:
$VDEMO_component_people_tracker
$VDEMO_component_openpose
default:
"

# MISC
export VDEMO_component_bart="roslaunch,$laptop, -t bart -v PACKAGE=tobi_bringup -v LAUNCHFILE=bart_meka.launch -L 9:"


#handover adaption
export VDEMO_component_jacobian_control="roslaunch,$upper, -t jacobian_control -v PACKAGE=jacobian_control -v LAUNCHFILE=jacobian_control_node.launch -L 9:"
export VDEMO_component_ia_goal_uploader="roslaunch,$upper, -t ia_goal_uploader -v PACKAGE=goal_uploader -v LAUNCHFILE=interactive_marker_uploader.launch -n -L 9:"
export VDEMO_component_goal_uploader="roslaunch,$upper, -t goal_uploader -v PACKAGE=goal_uploader -v LAUNCHFILE=goal_uploader.launch -n -L 9:"


export VDEMO_component_group_handover_adaption="
adaption:
$VDEMO_component_handtracking
$VDEMO_component_otpprediction
$VDEMO_component_jacobian_control
$VDEMO_component_ia_goal_uploader
$VDEMO_component_goal_uploader
default:
"

# GAZE DEMO
export VDEMO_component_gaze_relay="rosrun,$upper,-l -t gaze_relay -g gaze_demo -v PACKAGE=gaze_relay -v BINARY=gaze_relay_node.py -L 6:"
export VDEMO_component_opfa="rosrun,$upper,-l -t opfa -g gaze_demo -v PACKAGE=openface2_bridge -v BINARY=headpose_single -v NODE_TO_CHECK=/openface2_ros -L 6 -x:"

##############################################################################
# GROUPS MEKA
##############################################################################


export VDEMO_component_group_net="
$VDEMO_component_roscore
$VDEMO_component_spread_upper
$VDEMO_component_spread_lower
$VDEMO_component_spread_laptop
"

export VDEMO_component_group_meka_default="
$VDEMO_component_roscore
$VDEMO_component_group_ros
$VDEMO_component_spread_upper
$VDEMO_component_spread_lower
$VDEMO_component_spread_laptop
$VDEMO_component_group_m3
$VDEMO_component_group_calib
$VDEMO_component_group_robot
hardware:
$VDEMO_component_ros_controller_stiff
default:
"

export VDEMO_component_group_floka_default="
$VDEMO_component_roscore
$VDEMO_component_group_ros
$VDEMO_component_group_m3
$VDEMO_component_group_calib
$VDEMO_component_group_robot
hardware:
$VDEMO_component_ros_controller_stiff
default:
"
