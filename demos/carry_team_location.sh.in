# Find out the local directory this script lives in:
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/base.sh"

if [ "$VDEMO_simulation" = true ]; then
##############################################################################
## VIRTUAL ROBOT
##############################################################################

#pocketsphinx_grammars
export PATH_TO_PSA_CONFIGS="/home/tmarkmann/robocup/robocup-speechrec/psConfig"
export VDEMO_PSA_CONFIG="${PATH_TO_PSA_CONFIGS}/tasks/helpmecarrySSPL.conf"

#Map
export PATH_TO_MAPS="${prefix}/share/robocup_data/maps"
export NAVIGATION_MAP="${PATH_TO_MAPS}/team_area.yaml"

# KBase
export PATH_TO_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/use_sim_testing_db.yaml"

#espeak
export VDEMO_component_espeak__="$VDEMO_component_espeak"

## /VIRTUAL ROBOT
#################
else
##############################################################################
## REAL ROBOT
##############################################################################

#pocketsphinx_grammars
export VDEMO_PSA_CONFIG="${PATH_TO_PSA_CONFIG}/tasks/helpmecarrySSPL.conf"

#Map
export PATH_TO_MAPS="${prefix}/share/robocup_data/maps"
export NAVIGATION_MAP="${PATH_TO_MAPS}/team_area.yaml"

#KBASE
export PATH_TO_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/use_team_area_db.yaml"

## /REAL ROBOT
##############
fi

##############################################################################
# TASK SPECIFIC CONFIGURATION VARIABLES
##############################################################################
export PATH_TO_BONSAI_TIAGO_CONFIG="${vdemo_prefix}/opt/bonsai_tiago_addons/etc/bonsai_configs"

#Bonsai Task
export PATH_TO_BONSAI_CONFIG="${PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG}/DefaultTiagoConfig.xml"
export PATH_TO_BONSAI_TASK="${PATH_TO_BONSAI_ROBOCUPTASKS_SCXML}/tasks/carryMyLuggage.xml"

#people
export VDEMO_component_people_tracker="roslaunch, $laptop, -v PACKAGE=tobi_sim -v LAUNCHFILE=people_tracker.launch -n -t people_tracker -l -g NAV -L 9:"
export VDEMO_component_openpose_tiago="roslaunch,$upper, -t openpose -g people -v PACKAGE=openpose_ros -v LAUNCHFILE=openpose_ros_tiago.launch -v ARGS=\"camera_rgb_topic\:=\${RGB_CAM_TOPIC} camera_depth_topic\:=\${DEPTH_CAM_TOPIC}\" -L 7:"

#tracking
export VDEMO_component_legdetector_mod="roslaunch, $laptop, -v PACKAGE=leg_detector -v LAUNCHFILE=leg_detector_tiago.launch -t leg_detector -l -g PEOPLE -L 9:"
export VDEMO_component_cftld_na="roslaunch, $laptop, -v PACKAGE=cf_tld_ros -v LAUNCHFILE=tiago_tracking.launch -t CFtld -l -g PEOPLE -L 9:"
export VDEMO_component_group_tracking_na="
tracking:
$VDEMO_component_bayes_tracker
$VDEMO_component_legdetector_mod
$VDEMO_component_cftld_na
"

#navigation
export VDEMO_component_move_base="roslaunch, $laptop, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=move_base_following.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -t move_base -l -g NAV -L 9:"
export VDEMO_component_amcl="roslaunch, $laptop, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=amcl.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -t amcl -l -g NAV -L 9:"
export VDEMO_component_map_server="roslaunch, $laptop, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=map_server.launch -v ARGS=\"map\:=\${NAVIGATION_MAP}\" -t map -l -g NAV -L 9:"

#export shelf_mode=true

export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
export NAVIGATION_WORLD="furnished_clf_actor"

##############################################################################
# COMPONENT LIST
##############################################################################
export VDEMO_components="

$VDEMO_component_group_tiago_base
$VDEMO_component_group_kbase
$VDEMO_component_group_tracking_na
$VDEMO_component_openpose_tiago
$VDEMO_component_group_viz
$VDEMO_component_group_tools

nav:
$VDEMO_component_move_base
$VDEMO_component_amcl
$VDEMO_component_map_server

bonsai:
$VDEMO_component_bonsai_robocup

deprecated:
$VDEMO_component_people_tracker

handover:
$VDEMO_component_hand_over
$VDEMO_component_moveit

speech:
$VDEMO_component_speech_rec
$VDEMO_component_ps_web_gui
$VDEMO_component_ros_websocket
$VDEMO_component_espeak__
"

#$VDEMO_component_group_speech

