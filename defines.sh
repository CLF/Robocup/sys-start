##############################################################################
##############################################################################
#########	             COMPONENT DEFINES                      ##########
##############################################################################
##############################################################################

#default language
export language=english

#default robot type, overwrite in robot_setup file
if [ -z "$ROBOT_TYPE" ]; then
export ROBOT_TYPE=meka
fi

# Allow the creation of core files. They should appear in your home directory.
# Use gdb -core <core file> for backtracing.
# ulimit -c 0


##############################################################################
# PC Setup 
##############################################################################

# defaults (overwritten by robot setup)
export basepc=`hostname -s`
export upper=$basepc
export lower=$basepc
export gpu=$basepc
export laptop=$basepc
export robot=$basepc


##############################################################################
# MIDDLEWARE CONFIG 
##############################################################################

# defaults (overwritten by robot setup)
export VDEMO_robot_config_loaded="FALSE"
export RSB_PLUGINS_CPP_LOAD=rsbspread
export RSB_TRANSPORT_SPREAD_PORT=4803
export RSB_TRANSPORT_SPREAD_HOST=localhost
export RSB_TRANSPORT_SPREAD_ENABLED=1
export RSB_TRANSPORT_SOCKET_ENABLED=0
export RSB_TRANSPORT_INPROCESS_ENABLED=0
export spreadhost=$lower
export SPREADCONFIG=${vdemo_prefix}/etc/spread/$basepc
export ROS_MASTER_URI=http://$basepc:11311

# ros params
export NAVIGATION_USE_MAP=false
export NAVIGATION_USE_SIM=$VDEMO_simulation
export MANIPULATION_USE_SIM=$VDEMO_simulation
export ROS_CURRENT=kinetic
export ROS_LEGACY=indigo

##############################################################################
# BONSAI-2
##############################################################################


# Path to bonsai config
export PATH_TO_BONSAI_ROBOCUP_CONFIG="${vdemo_prefix}/opt/bonsai_robocup_addons/etc/bonsai_configs"
export PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG="${vdemo_prefix}/opt/bonsai_robocup_exercise/etc/bonsai_configs"
export PATH_TO_BONSAI_TIAGO_CONFIG="${vdemo_prefix}/opt/bonsai_tiago_addons/etc/bonsai_configs"

# Path to scxml locations
export PATH_TO_BONSAI_ROBOCUP_SCXML="${vdemo_prefix}/opt/bonsai_robocup_addons/etc/state_machines"
export PATH_TO_BONSAI_CORE_SCXML="${prefix}/opt/bonsai-scxml_engine/etc/behaviors"
export PATH_TO_BONSAI_ROBOCUPTASKS_SCXML="${prefix}/opt/bonsai_robocup_exercise/etc/state_machines"
export PATH_TO_BONSAI_PEPPER_SCXML="${prefix}/opt/bonsai2-pepper-dist/etc/state_machines"
export PATH_TO_BONSAI_TIAGO_SCXML="${prefix}/opt/bonsai_tiago_addons/etc/state_machines"

# Create mapping variable, used by bonsai2 to resolve "src=" attributes in scxml files
export BONSAI_MAPPING="ROBOCUP=${PATH_TO_BONSAI_ROBOCUP_SCXML} SCXML=${PATH_TO_BONSAI_CORE_SCXML} EXERCISE=${PATH_TO_BONSAI_ROBOCUPTASKS_SCXML} PEPPER=${PATH_TO_BONSAI_PEPPER_SCXML} TIAGO=${PATH_TO_BONSAI_TIAGO_SCXML}"

##############################################################################
# COMPONENT VARIABLES 
##############################################################################

# location of World data
export PATH_TO_MAPS="${vdemo_prefix}/share/maps"
export PATH_TO_ANNOTATIONS="${vdemo_prefix}/share/annotations"
export PATH_TO_PSA_CONFIG="${vdemo_prefix}/share/SpeechRec/psConfig"

export PATH_TO_CLASSIFIERS="${vdemo_prefix}/share/tensorflow"
export PATH_TO_OBJECT_MODELS="${vdemo_prefix}/share/clafu/models"

export DEFAULT_MODELS="/rc2017/magdeburg/all"
export OBJECT_MODELS="${PATH_TO_OBJECT_MODELS}/${DEFAULT_MODELS}"

#default map
export DEFAULT_MAP="centralLab"
export NAVIGATION_MAP="${PATH_TO_MAPS}/magdeburgArena_innen"
export DEFAULT_ANNOTATIONS="annotations_centralLab.xml"
export DEBUG_MODE="FALSE"

export NAVIGATION_WORLD="${PATH_TO_WORLDS}/${DEFAULT_MAP}"
export NAVIGATION_MAP="${PATH_TO_MAPS}/${DEFAULT_MAP}"

# camera topics
if [ -z "$RGB_CAM_TOPIC" ]; then
export RGB_CAM_TOPIC=/xtion/rgb
fi
if [ -z "$DEPTH_CAM_TOPIC" ]; then
export DEPTH_CAM_TOPIC=/xtion/depth_registered
fi

export OBJECT_MERGER_THRESHOLD=0.5

# segmentation
export SEGMENTATION_CONFIGS="${vdemo_prefix}/share/clf_object_recognition_config/config"
export KINECT_COLOR_XML="${SEGMENTATION_CONFIGS}/kinect-color-vga.xml"
export KINECT_DEPTH_XML="${SEGMENTATION_CONFIGS}/kinect-depth-vga.xml"
export SEGMENTER_PATH="${SEGMENTATION_CONFIGS}/segmentation_config.xml"
export ROBOT_FILTER="${SEGMENTATION_CONFIGS}/robotfilter.xml"

# Default KBASE locations
export PATH_TO_MONGOD_CONFIG="$prefix/share/robocup_data/mongod/mongod.conf"
export PATH_TO_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/empty_tmp_db.yaml"
export PATH_TO_EDIT_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/empty_tmp_db.yaml"

##############################################################################
# PATHS
##############################################################################
export PATH="${vdemo_prefix}/bin:$PATH"
export PYTHONPATH=$PYTHONPATH:${vdemo_prefix}/lib/python2.7/site-packages
