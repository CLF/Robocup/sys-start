## Tiago defines
export PATH_TO_MAPS="${prefix}/share/robocup_data/maps"

##############################################################################
# COMPONENT LIST TIAGO
##############################################################################

if [ "$VDEMO_simulation" = true ]; then
    export ROBOT_VERSION="steel"
##############################################################################
## VIRTUAL ROBOT
##############################################################################
	echo "prefix: $prefix"
	export VDEMO_component_gazebo_tiago_custom="roslaunch,$lower, -t gazebo_tiago -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=tiago_gazebo_clf.launch -v ARGS=\"world\:=\${NAVIGATION_WORLD}\" -x -l -L 2:"
	export VDEMO_component_gazebo_spawn="roslaunch,$lower, -t gazebo_spawn -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=tiago_spawn.launch -v ARGS=\"y\:=4.0\" -l -L 2:"
	export VDEMO_component_bringup="roslaunch,$lower, -t bringup -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=tiago_bringup.launch -l -L 3:"
	export VDEMO_component_rqt_control="roslaunch,$lower, -t rqt_controller -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=tiago_rqt_controller.launch -n -l -L 5:"

	export VDEMO_component_group_sim="
	sim:
	$VDEMO_component_gazebo_tiago_custom
	$VDEMO_component_gazebo_spawn
	$VDEMO_component_bringup
	$VDEMO_component_rqt_control
	"

	#Speech
	export VDEMO_component_espeak="rosrun, $basepc, -v NODE_TO_CHECK=espeak_node -v PACKAGE=espeak_ros -v BINARY=espeak_node -t espeak -l -g TTS -L 2:"

	# SENSORS
	export VDEMO_component_xtion2="roslaunch,$upper, -n -l -t xtion_cam -g hw -v PACKAGE=tobi_sim -v LAUNCHFILE=xtion2.launch -L 2: "

	## Viz
	export VDEMO_component_tiago_rviz="ros_rviz,$laptop, -n -x -g ros -v RVIZ_CONFIG=${prefix}/share/tobi_sim/config/tiago.rviz -L 5: "

	## Navigation
	export VDEMO_component_nav="roslaunch, $robot, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=nav.launch -v ARGS=\"map_file\:=\${NAVIGATION_MAP} sim\:=$VDEMO_simulation\" -t nav -l -g NAV -L 9:"
	export VDEMO_component_move_base="roslaunch, $robot, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=move_base.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -n -t move_base -l -g NAV -L 9:"
	export VDEMO_component_mapping="roslaunch, $robot, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=gmapping.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -n -t mapping -l -g NAV -L 9:"
	export VDEMO_component_amcl="roslaunch, $robot, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=amcl.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -n -t amcl -l -g NAV -L 9:"
	export VDEMO_component_map_server="roslaunch, $robot, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=map_server.launch -v ARGS=\"map\:=\${NAVIGATION_MAP}\" -n -t map -l -g NAV -L 9:"

	## Garbage Grasping
	export VDEMO_component_obstacle_detector="roslaunch, $laptop, -v PACKAGE=obstacle_detector -v LAUNCHFILE=garbage_detector.launch -t obstacle_extractor -L 8:"

	## Tools
	export VDEMO_component_actor_teleop="teleop,$basepc, -n -t gz_actor_teleop -l -g TOOLS -v ARGS=\"_topic\:=\${ACTOR_VEL_TOPIC}\" -x -L 9: "

################
## /VIRTUAL ROBOT
#################

else
	##############################################################################
	## REAL ROBOT
	##############################################################################
	# Roscore always is already running on tiago
	export VDEMO_component_roscore=""

    ## Learning Server and GUI
    export VDEMO_component_simple_learn="roslaunch, $laptop, -v PACKAGE=simple_learning -v LAUNCHFILE=simple_learning_server.launch -n -t learning_server -g TOOLS -L 9:"
    export VDEMO_component_simple_learn_gui="rosrun, $laptop, -v PACKAGE=learning_gui -v BINARY=learning_gui.py -n -t learning_GUI -g TOOLS -x -L 9:"

	export VDEMO_component_tiago_webcommander="tiago_webcommander,$laptop, -t webcommander -n -x -g ros -L 5: "

	export VDEMO_component_speech_tiago="speech_rec,$robot, -t pocketsphinx_tiago -g speech -v alsa_device=plug_tiago_mono -L 3 -w 2 : "
	export VDEMO_component_speech_rec="speech_rec,$laptop, -t pocketsphinx -g speech -L 3 -w 2 : "

	## Navigation
	export VDEMO_component_move_base="roslaunch, $laptop, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=move_base.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -t move_base -l -g NAV -L 9:"
	export VDEMO_component_mapping="roslaunch, $robot, -v PACKAGE=tiago_2dnav -v LAUNCHFILE=mapping.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -t mapping -l -g NAV -L 9:"
	export VDEMO_component_amcl="roslaunch, $laptop, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=amcl.launch -v ARGS=\"sim\:=$VDEMO_simulation\" -t amcl -l -g NAV -L 9:"
	export VDEMO_component_map_server="roslaunch, $laptop, -v PACKAGE=tiago_clf_nav -v LAUNCHFILE=map_server.launch -v ARGS=\"map\:=\${NAVIGATION_MAP}\" -t map -l -g NAV -L 9:"
	export VDEMO_component_pose_publisher="rosrun,$robot, -t pose_publisher -g posture -v PACKAGE=robot_pose_publisher -v BINARY=robot_pose_publisher -L 4:"

	## Viz
#	export VDEMO_component_tiago_rviz="ros_rviz,$laptop, -n -x -g ros -v RVIZ_CONFIG=${prefix}/share/tobi_sim/config/tiago.rviz -L 5: "
	export VDEMO_component_tiago_rviz_clf="ros_rviz,$laptop, -t rviz_clf_nav -n -x -g ros -v RVIZ_CONFIG=${prefix}/share/tiago_clf_nav/config/rviz.rviz -L 5: "
	export VDEMO_component_tiago_rviz_mini="ros_rviz,$laptop, -t rviz_minimal -n -x -g ros -v RVIZ_CONFIG=${prefix}/share/robocup_data/rviz/tiago_minimal.rviz -L 5: "

	## Manipulation
	export VDEMO_component_moveit="roslaunch, $basepc, -v PACKAGE=tiago_moveit_config -v LAUNCHFILE=move_group.launch -t move_group -l -g ARM -L 2: "
	export VDEMO_component_handover_rot="roslaunch, $laptop, -t handover_rot  -v PACKAGE=hand_over_tiago -v LAUNCHFILE=hand_over_rot.launch -l  -L 5: "

	## Garbage Grasping
	export VDEMO_component_obstacle_detector="roslaunch, $laptop, -v PACKAGE=obstacle_detector -v LAUNCHFILE=garbage_detector_robot.launch -t obstacle_extractor -L 8:"

## /REAL ROBOT
##############
fi
##############################################################################
# Additional components
##############################################################################
export VDEMO_component_xtion_relay="roslaunch, $laptop, -v PACKAGE=tiago_clf_launch -v LAUNCHFILE=xtion_relay_endpoint.launch -t xtion_relay -l -g ros -L 1:"

# todo better sonar cloud



## People
export VDEMO_component_bayes_tracker="roslaunch, $laptop, -v PACKAGE=bayes_people_tracker -v LAUNCHFILE=people_tracker.launch -t bayes_tracker -l -g PEOPLE -L 9:"
export VDEMO_component_legdetector="roslaunch, $laptop, -v PACKAGE=leg_detector -v LAUNCHFILE=leg_detector_tiago.launch -t leg_detector -l -g PEOPLE -L 9:"
export VDEMO_component_cftld="roslaunch, $laptop, -v PACKAGE=cf_tld_ros -v LAUNCHFILE=tiago_tracking.launch -t CFtld -l -g PEOPLE -L 9:"

# KBASE
export VDEMO_component_mongod="mongod, $basepc,-l -t P_mongod -g KBASE -w 40 -v ARGS=\"\${PATH_TO_MONGOD_CONFIG}\" -L 8:"
export VDEMO_component_mappr_tmp="roslaunch, $basepc, -v PACKAGE=mappr_server_kbase -v LAUNCHFILE=mappr_server_kbase.launch -t use_kbase -v ARGS=\"kbase_config_path\:=\${PATH_TO_KBASE_CONFIG}\" -l -g KBASE -L 9:"
export VDEMO_component_mappr_edit="roslaunch, $basepc, -v PACKAGE=mappr_server_kbase -v LAUNCHFILE=mappr_server_kbase.launch -t edit_kbase -v ARGS=\"kbase_config_path\:=\${PATH_TO_EDIT_KBASE_CONFIG}\" -l -n -g KBASE -L 9:"

## Bonsai
export VDEMO_component_bonsai_tiago="bonsai_tiago,$laptop, -g bonsai -t bonsai_tiago -x -d 0 -l -w 5 -L 9:"
export VDEMO_component_bonsai_robocup="bonsai_robocup,$laptop, -g bonsai -t bonsai_robocup -x -d 0 -l -w 5 -L 9:"

## Object
export VDEMO_component_tf_recognition="tensorflow, $gpu, -l -t tf_recognition -g object -v PACKAGE=clf_object_recognition_tensorflow -v BINARY=object_recognition_node -v ARGS=\"_graph_path\:=\${OBJECT_REC_GRAPH} _labels_path\:=\${OBJECT_REC_LABELS} \" -L 7:"
export VDEMO_component_tf_detection="tensorflow_detection, $gpu, -l -t tf_detection -g object -v PACKAGE=clf_object_recognition_tensorflow -v BINARY=object_detection_node -v ARGS=\"_graph_path\:=\${OBJECT_DET_GRAPH} _labels_path\:=\${OBJECT_DET_LABELS} _rec_path\:=\${OBJECT_REC_PATH} \" -L 7:"

# Segmentation
export VDEMO_component_object_merger="object_merger, $gpu, -t object_merger -g object -l -L 7:"
export VDEMO_component_sq_fitting="roslaunch, $laptop, -v PACKAGE=sq_fitting_adapter -v LAUNCHFILE=sq_fit.launch -n -t sq_fitting -l -g object -L 2:"
## ICL
export VDEMO_component_segmentation="iclros_segmentation, $gpu, -t segmentation -g segmentation -L 7:"
export VDEMO_component_iclros_bridge="iclros_bridge, $gpu, -t iclros_bridge -g segmentation -L 7:"

# Speech
export VDEMO_component_ps_web_gui="ps_web_gui,$basepc, -g speech -t web_gui -n -x -d 0 -l -w 5 -L 9:"

# Hand_over
export VDEMO_component_hand_over="roslaunch,$laptop, -t hand_over -g sim -v PACKAGE=hand_over_tiago -v LAUNCHFILE=hand_over.launch -l -L 3:"
# todo learning gui
# todo learning server

#Grasping
export VDEMO_component_tiago_rviz_mtc="ros_rviz,$laptop, -t rviz_mtc -n -x -g ros -v RVIZ_CONFIG=${prefix}/share/clf_mtc_server/config/tiago.rviz -L 5: "
export VDEMO_component_mtc_server_tiago="roslaunch,$laptop, -t mtc_server -g sim -v PACKAGE=clf_mtc_server -v LAUNCHFILE=tiago.launch -l -L 3:"
export VDEMO_component_handover_rot="roslaunch, $laptop, -t handover_rot  -v PACKAGE=hand_over_tiago -v LAUNCHFILE=hand_over_rot.launch -l  -L 2: "

# Garbage Grasping
export VDEMO_component_garbage_server="rosrun, $laptop, -v NODE_TO_CHECK=garbage_grasping_server -v PACKAGE=garbage_grasping_server -v BINARY=detect.py -l -t garbage_server -L 8:"
export VDEMO_component_garbage="roslaunch, $laptop, -t garbage_server_grasp -v PACKAGE=garbage_grasping_server -v LAUNCHFILE=garbage_server.launch -l  -L 8: "


# Tools
export VDEMO_component_teleop="teleop,$basepc, -n -t teleop -l -g TOOLS -v ARGS=\"_topic\:=/key_vel\" -L 9:"
export VDEMO_component_play_motion_gui="rosrun, $basepc, -x -v PACKAGE=actionlib -v BINARY=axclient.py -v ARGS="/play_motion" -n -t play_motion-client -g TOOLS -L 9:"

##############################################################################
# GROUP LIST
##############################################################################

## redefine default groups
export VDEMO_component_group_person="
person:
$VDEMO_component_gender_and_age
$VDEMO_component_openpose
"

export VDEMO_component_group_tracking="
tracking:
$VDEMO_component_bayes_tracker
$VDEMO_component_legdetector
$VDEMO_component_cftld
"

export VDEMO_component_group_garbage_server="
garbage:
$VDEMO_component_obstacle_detector
$VDEMO_component_garbage_server
$VDEMO_component_garbage
"

####

#redefine as we change speech_rec&&tts
export VDEMO_component_group_speech="
speech:
$VDEMO_component_speech_rec
$VDEMO_component_espeak
$VDEMO_component_ros_websocket
$VDEMO_component_speech_webgui
"

export VDEMO_component_group_viz="
viz:
$VDEMO_component_tiago_rviz_mini
$VDEMO_component_tiago_rviz
$VDEMO_component_tiago_rviz_clf
$VDEMO_component_tiago_rviz_mtc
$VDEMO_component_tiago_webcommander
"

export VDEMO_component_group_tiago_base="
$VDEMO_component_group_sim
default:
$VDEMO_component_roscore
$VDEMO_component_xtion_relay
"

export VDEMO_component_group_navigation="
nav:
$VDEMO_component_nav
$VDEMO_component_move_base
$VDEMO_component_amcl
$VDEMO_component_map_server
"

export VDEMO_component_group_mapping="
nav:
$VDEMO_component_mapping
$VDEMO_component_move_base
"

export VDEMO_component_group_bonsai="
bonsai:
$VDEMO_component_bonsai_robocup
"

export VDEMO_component_group_manipulation="
arm:
$VDEMO_component_moveit
$VDEMO_component_mtc_server_tiago
$VDEMO_component_hand_over
"

export VDEMO_component_group_object="
object:
$VDEMO_component_tf_recognition
$VDEMO_component_tf_detection
$VDEMO_component_tf_annotate
$VDEMO_component_tf_test
$VDEMO_component_tf_train
"

export VDEMO_component_group_segmentation="
segmentation:
$VDEMO_component_segmentation
$VDEMO_component_iclros_bridge
$VDEMO_component_object_merger
$VDEMO_component_sq_fitting
"

export VDEMO_component_group_tools="
tools:
$VDEMO_component_teleop
$VDEMO_component_actor_teleop
$VDEMO_component_play_motion_gui
$VDEMO_component_simple_learn
$VDEMO_component_simple_learn_gui
"
export VDEMO_component_group_kbase="
kbase:
$VDEMO_component_mongod
$VDEMO_component_mappr_tmp
$VDEMO_component_mappr_edit
"
