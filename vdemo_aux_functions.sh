function vdemo_get_logdir {
	local VDEMO_demoid="${BASH_SOURCE[${#BASH_SOURCE[@]}-1]##*/}"
	if [ "$VDEMO_demoid" = "vdemo2" ]; then
		VDEMO_demoid="${BASH_SOURCE[${#BASH_SOURCE[@]}-2]##*/}"
	fi
	echo "/tmp/vdemo-$USER-${VDEMO_demoid%.sh}"
}

function vdemo_gdbbacktrace {
    tracelog="${VDEMO_logfile_prefix}${0##*/}_trace.log"
    date +"++++ %Y-%m-%d %T ++++" >> "$tracelog"
    echo "$@" >> "$tracelog"
    gdb -q -n -return-child-result -ex "set confirm off" -ex "set logging file $tracelog" -ex "set logging on" -ex "set logging redirect on" -ex "handle SIGTERM noprint nostop" -ex "set pagination off" -ex run -ex "thread apply all bt" -ex "quit" --args "$@"
    date +$'---- %Y-%m-%d %T ----\n' >> "$tracelog"
}
export -f vdemo_gdbbacktrace

function vdemo_coredump {
    coredumpdir="$VDEMO_logdir/${0##*/}"
    mkdir -p "$coredumpdir" && cd "$coredumpdir" && ulimit -c unlimited && "$@"
}
export -f vdemo_coredump

function vdemo_inspect_cmd {
    rsb-loggerclmaster -s timeline/scope "spread:$OUTSCOPE"
    echo "***logger terminated***"
    read
}
export -f vdemo_inspect_cmd

# This function schedules a component stop. First of all a SIGINT is sent to the related process. After the SIGINT_TIMEOUT a SIGTERM is send. After the SIGTERM_TIMEOUT the process is kill via SIGKILL.
# The default timeouts are listed below. You can adjust the related timeouts via arguments as well. Timeouts are defined as float values representing seconds.
#
# Syntax: vdemo_scheduled_component_stop SCREEN_PID DEFAULT_SIGINT_TIMEOUT DEFAULT_SIGTERM_TIMEOUT
# Example: vdemo_scheduled_component_stop $1 6 6
#

function vdemo_scheduled_component_stop() {

    DEFAULT_SIGINT_TIMEOUT=5
    DEFAULT_SIGTERM_TIMEOUT=5
    
    # overwrite default values by given arguments.
    
    SIGINT_TIMEOUT=${2:-${DEFAULT_SIGINT_TIMEOUT}}
    SIGTERM_TIMEOUT=${3:-${DEFAULT_SIGTERM_TIMEOUT}}
        
    # Upscale timeouts to optimize return response by minimizing iteration step.
    
    SIGINT_TIMEOUT=`expr $SIGINT_TIMEOUT \* 10`
    SIGTERM_TIMEOUT=`expr $SIGTERM_TIMEOUT \* 10`
    
    # convert floor values to guarantee loop termination.
    
    SIGINT_TIMEOUT=`printf "%.0f" $SIGINT_TIMEOUT`
    SIGTERM_TIMEOUT=`printf "%.0f" $SIGTERM_TIMEOUT`

    local PIDS=$(all_children $(vdemo_pidFromScreen $1))
    
    echo "stop processes $PIDS"
    kill -SIGINT $PIDS > /dev/null 2>&1
    for ((i=1; i<=$SIGINT_TIMEOUT; ++i)); do
        sleep 0.1
        kill -0 $PIDS > /dev/null 2>&1 || return
    done
    
    echo "terminate processes $PIDS"
    kill -SIGTERM $PIDS > /dev/null 2>&1
    for ((i=1; i<=$SIGTERM_TIMEOUT; ++i)); do
        sleep 0.1
        kill -0 $PIDS > /dev/null 2>&1 || return
    done
    
    echo "killing processes $PIDS"
    kill -SIGKILL $PIDS  > /dev/null 2>&1
}
export -f vdemo_scheduled_component_stop

function rsb_monitor {

	cmd=""
	for str in "$@"
	do
	    cmd="$cmd \"$str\""
	done

    echo "will launch $cmd"
    eval "$cmd &"
    pid=$!
    echo "Launched PID ${pid}"
    name=$(echo $STY | sed 's/.*\.\(.*\)_/\1/')
    ${prefix}/bin/rsb-process-monitor0.11 -s -i -r -u $pid -n "$name" &
    fg %-

}
export -f rsb_monitor

function ros_env {
    export ROSCONSOLE_FORMAT='[${severity}] [${logger}@${time}]: ${message}'
    source ${prefix}/setup.bash
}
export -f ros_env
