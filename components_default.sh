##############################################################################
# DEFAULT COMPONENT SETUP
##############################################################################
# set components to start on which host, format as follows:munch
# component specifiers seperated by ': '
# each component specifier has three fields seperated by ',':
#    1. a host name where the component should run
#    2. components name: name of the component, a respective component
#       script for must be available in $VDEMO_scripts, with the
#       following name: component_<component>.sh
#    3. optional argument for automatic start (wait time, wait for XCF
#       registration...)
#       known option include:
#   Options:
#      -w <n> wait n seconds for process to start completely
#      -W <n> set delay when an asynchronous check is performed in case -w is not specified
#      -l     activate initial logging for the component
#      -x     use own X server for the component
#      -n     do not include in level/group/auto start
#      -g <s> allow to define a group (string: name of group)
#      -L <n> component level, affects starting order (numeric: level)
#      -d <n> detach time, automatically detaches screen after n seconds, or
#             leaves it open all the time (-1), default is 10 seconds
#      -t     title of the component / provide unique names for multiple instances on the same host
#      -v     export variable varname=var to component script
#
# BE CAREFUL: No character after line end ('\') because the shell is very picky
# about that!!!

##############################################################################
# COMPONENT LIST
##############################################################################

# NET
export VDEMO_component_spread="spread,$basepc, -g rsb -L 0 -w 2:"
export VDEMO_component_spread_upper="spread,$upper,-g rsb -L 0 -w 2:"
export VDEMO_component_spread_lower="spread,$lower, -g rsb -L 0 -w 2:"
export VDEMO_component_spread_laptop="spread,$basepc, -g rsb -L 0 -w 2:"
export VDEMO_component_roscore="roscore,$upper, -t roscore -g ros -w 10 -L 0: "

# SPEECH
export VDEMO_component_tts="text_to_speech,$upper, -g speech -w 2 -L 3 : "
export VDEMO_component_speech_rec="speech_rec,$upper, -t pocketsphinx -g speech -L 3 -w 2 -x: "
export VDEMO_component_speech_test="rsb_send_string,$upper, -t speech_test -v MESSAGE=\"test\. one two\. one two\" -n -v SCOPE=/speech/tts/mary/ -L 5 -g speech -w 1 : "
export VDEMO_component_speech_webgui="pocketsphinx_adapter_web,$laptop, -n -t speech_gui -g speech -L 4 :"

# GAZEBO
export VDEMO_component_gazebo="gazebo_server,$lower, -t gazebo -g sim -n -L 1 -w 4: "
export VDEMO_component_gazebo_gui="roslaunch,$lower, -t gazebo_gui -x -g sim -v PACKAGE=tobi_sim -v LAUNCHFILE=furnished_clf.launch -L 1 -w 4: "

# ROS/RSB CONVERSION
export VDEMO_component_ros_rct_bridge="ros_rct_bridge,$upper, -g nav -L 4: "
# ROS/WEBBRIDGE
export VDEMO_component_ros_websocket="roslaunch,$basepc, -v PACKAGE=rosbridge_server -v LAUNCHFILE=rosbridge_websocket.launch -t rosbridge_websocket -g ros -L 1:"

# MEMORY
export VDEMO_component_memory="memory,$upper, -g bonsai -L 1 -w 2: "

# BONSAI
export VDEMO_component_bonsai_robocup="bonsai_robocup,$upper, -g bonsai -t bonsai -x -d 0 -l -w 5 -L 9:"
export VDEMO_component_bonsai_rsbgui="bonsai_robocup_rsbgui,$upper, -g bonsai -t bonsai_rsb_gui -x -d 0 -l -w 5 -L 9:"
export VDEMO_component_bonsai_rsbserver="bonsai_robocup_rsbserver,$basepc, -g bonsai -t bonsai_rsb_server -x -d 0 -l -w 5 -L 9:"

# FlexBE
export VDEMO_component_flexbe_app="roslaunch,$upper, -g flexbe -t flexbe_app -v PACKAGE=flexbe_app -v LAUNCHFILE=flexbe_full.launch -x -d 0 -l -w 5 -L 9:"

# PEOPLE
export VDEMO_component_openpose="roslaunch,$upper, -t openpose -g people -v PACKAGE=openpose_ros -v LAUNCHFILE=openpose_ros.launch -v ARGS=\"camera_rgb_topic\:=\${RGB_CAM_TOPIC} camera_depth_topic\:=\${DEPTH_CAM_TOPIC}\" -L 7:"
export VDEMO_component_gender_and_age="openpose_gender_and_age,$lower, -n -l -g people -w 5 -L 6:"
export VDEMO_component_leg_detector="roslaunch, $upper, -t leg_detector -g people -v PACKAGE=leg_detector -v LAUNCHFILE=leg_detector.launch -L 7:"
##todo people tracker

# MISC
export VDEMO_component_record_data="record_data,$upper, -x -d 0 -g recording -w 5 -n -L 50:"

# SENSORS
#export VDEMO_component_xtion2="roslaunch,$upper, -t xtion_cam -g hw -v PACKAGE=tobi_sim -v LAUNCHFILE=xtion2.launch -L 2: "

# OBJECT
export VDEMO_component_tensorflow="tensorflow, $gpu, -t tensorflow -g object -v PACKAGE=tensorflow_ros -v BINARY=object_recognition_node -v ARGS=\"_graph_path\:=\${OBJECT_REC_GRAPH} _labels_path\:=\${OBJECT_REC_LABELS} \" -L 7:"
export VDEMO_component_tf_annotate="tf_annotation_tool,$basepc, -n -t tf_annotation -l -g object -L 7:"
export VDEMO_component_tf_test="tf_test_tool,$basepc, -n -t tf_test -l -g object -L 7:"
export VDEMO_component_tf_train="tf_train_tool,$basepc, -n -t tf_training -l -g object -L 7:"

##############################################################################
# GROUP LIST
##############################################################################

export VDEMO_component_group_gazebo="
$VDEMO_component_gazebo
$VDEMO_component_gazebo_gui
"

export VDEMO_component_group_speech="
speech:
$VDEMO_component_tts
$VDEMO_component_speech_rec
$VDEMO_component_speech_test
$VDEMO_component_ros_websocket
$VDEMO_component_speech_webgui
"

export VDEMO_component_group_ros="
$VDEMO_component_roscore
$VDEMO_component_ros4rsb
"

export VDEMO_component_group_bonsai="
bonsai:
$VDEMO_component_bonsai_robocup
$VDEMO_component_bonsai_rsbserver
$VDEMO_component_bonsai_rsbgui
"

export VDEMO_component_group_openpose="
openpose:
$VDEMO_component_gender_and_age
$VDEMO_component_openpose
"

export VDEMO_component_group_person="
person:
$VDEMO_component_gender_and_age
$VDEMO_component_openpose
$VDEMO_component_leg_detector
"

export VDEMO_component_group_object="
object:
$VDEMO_component_tensorflow
$VDEMO_component_tf_annotate
$VDEMO_component_tf_test
$VDEMO_component_tf_train
"
